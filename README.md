# Tasklist

A simple task list application.

## Requirements

In order to build and run the application, you will need the following:

- [Git](https://git-scm.com/) scm
- [Java 11](https://jdk.java.net/11/) jdk
- [Maven](https://maven.apache.org/) repository
- [Docker](https://www.docker.com/) container technology (optional)
- [Postgres](https://www.postgresql.org/) database (optional)

Docker and postgres are mutually exclusive, you must use either one or the other.

Install these resources following the instructions provided in their respective sites.

## Compilation

- Clone this repository
```bash
git clone https://gitlab.com/jepdelablanca/tasklist.git
```
- compile the application
```bash
mvn clean
mvn install
```

## Installation with Docker

- run the docker container creation script
```bash
cd docker
sudo ./setup.sh
```

## Installation without Docker

- In a local postgres database, run the three scripts located at *tasklist-app/src/main/scripts*, in the order specified:
```bash
cd tasklist-app/src/main/scripts
psql -U postgres -f 01_database.sql
psql -U tasklist -f 02_schema.sql
psql -U tasklist -f 03_data.sql
```

Please note the scripts 2 and 3 must be run with the tasklist user.

## Running with Docker

- run the docker container startup script
```bash
cd docker
sudo ./run.sh
```

## Running without Docker

- _java -jar_ the compiled application
```bash
cd tasklist-app/target
java -jar tasklist-app.jar
```

## Usage

- Access the application via [http://localhost:8080](http://localhost:8080)
- Login using the guest credentials: _guest_:_guest_
- Access the Swagger REST api documentation via [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/)

## Contributing

This is a closed project, no contributions will be taken in consideration.

## License

[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
