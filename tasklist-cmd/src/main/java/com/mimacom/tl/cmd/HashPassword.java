package com.mimacom.tl.cmd;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Main class for the myrss commandline applcation.
 * 
 * This application should be an entry point to create passwords for users.
 *
 */
public class HashPassword {

	public static void main(String[] args) {
		System.out.println("Hash for password is:\n"+ BCrypt.hashpw(args[0], BCrypt.gensalt()));
	}
}
