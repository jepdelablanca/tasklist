#!/bin/bash

# Run database
docker run --name tasklist-database --network tasklist-network -p 5432:5432 -e POSTGRES_PASSWORD=2c5bD6dg01Faa1d8 -d tasklist/database

echo Waiting 5 seconds before connecting to the database...
sleep 5

# Execute database scripts
cd database
docker exec -i tasklist-database psql -U tasklist -f /02_schema.sql
docker exec -i tasklist-database psql -U tasklist -f /03_data.sql
cd ..

# Execute application
docker run --name tasklist-application --network tasklist-network -p 8080:8080 -d tasklist/application
