package com.mimacom.tl.neg;

import com.mimacom.tl.model.User;

public interface UserService {

	public String getUserName(String login);

	public boolean checkPassword(String password, User user);
}
