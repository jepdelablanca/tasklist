package com.mimacom.tl.neg;

import java.util.List;

import com.mimacom.tl.model.Task;

public interface TaskService {

	public List<Task> listUserTasks(String user);

	public Task createTask(Task task, String user);

	public Task updateTask(Task task, String user);

	public Task finalizeTask(Long id, String user);

	public void deleteTask(Long id, String user);

}