package com.mimacom.tl.neg;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mimacom.tl.dao.TaskRepository;
import com.mimacom.tl.exception.PermissionDeniedException;
import com.mimacom.tl.model.Task;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Override
	public List<Task> listUserTasks(String user) {
		return taskRepository.findByUser(user);
	}

	@Override
	public Task createTask(Task task, String user) {
		task.setId(null);
		task.setUser(user);
		return taskRepository.save(task);
	}

	@Override
	public Task updateTask(Task task, String user) {
		Task read = taskRepository.findById(task.getId()).get();
		if(!read.getUser().equals(user)) {
			throw new PermissionDeniedException();
		}
		read.updateValues(task);
		return taskRepository.save(read);
	}

	@Override
	public Task finalizeTask(Long id, String user) {
		Task read = taskRepository.findById(id).get();
		if(!read.getUser().equals(user)) {
			throw new PermissionDeniedException();
		}
		read.setCompletionDate(new Date());
		return taskRepository.save(read);
	}

	@Override
	public void deleteTask(Long id, String user) {
		Task read = taskRepository.findById(id).get();
		if(!read.getUser().equals(user)) {
			throw new PermissionDeniedException();
		}
		taskRepository.deleteById(id);
	}
}