package com.mimacom.tl.neg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mimacom.tl.dao.UserRepository;
import com.mimacom.tl.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public boolean checkPassword(String password, User user) {
		return passwordEncoder.matches(password, user.getPasswordHash());
	}

	@Override
	public String getUserName(String login) {
		return userRepository.findById(login).get().getName();
	}
}