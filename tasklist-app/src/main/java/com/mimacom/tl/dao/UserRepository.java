package com.mimacom.tl.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mimacom.tl.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

	Optional<User> findById(String id);
}
