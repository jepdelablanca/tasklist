package com.mimacom.tl.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Value("${tasklist.formLogin}")
	private Boolean formLogin;

	@Autowired
	public void initialize(AuthenticationManagerBuilder builder, DataSource dataSource) throws Exception {
		builder.jdbcAuthentication().dataSource(dataSource);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder)
			.usersByUsernameQuery("SELECT login, password_hash, enabled FROM users WHERE login = ?")
			.authoritiesByUsernameQuery("SELECT login, role FROM users WHERE login = ?");
	}

	@Override
	protected void configure(HttpSecurity security) throws Exception {
		if(Boolean.TRUE.equals(formLogin)) {
			security.authorizeRequests().antMatchers("/", "/tasks/*", "/me/*").authenticated().and().formLogin().and().csrf().disable().logout();
		} else {
			security.authorizeRequests().antMatchers("/", "/tasks/*", "/me/*").authenticated().and().httpBasic().and().csrf().disable();
		}
	}
}