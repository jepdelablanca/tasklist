package com.mimacom.tl.mvc;

import java.util.NoSuchElementException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mimacom.tl.exception.PermissionDeniedException;

@RestControllerAdvice
public class RequestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NoSuchElementException.class)
	protected ResponseEntity<Object> handleException(NoSuchElementException exception, WebRequest request) {
		return handleExceptionInternal(exception, "Element not found", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(RuntimeException.class)
	protected ResponseEntity<Object> handleException(RuntimeException exception, WebRequest request) {
		return handleExceptionInternal(exception, exception==null? "Unknown error": exception.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(PermissionDeniedException.class)
	protected ResponseEntity<Object> handleException(PermissionDeniedException exception, WebRequest request) {
		return handleExceptionInternal(exception, "You don't have permission to perform the specified action", new HttpHeaders(), HttpStatus.FORBIDDEN, request);
	}
}