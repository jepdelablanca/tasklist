package com.mimacom.tl.mvc;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mimacom.tl.model.Task;
import com.mimacom.tl.neg.TaskService;

@RestController
@Secured("user")
@RequestMapping("/tasks")
public class TaskController {

	private static final int MAX_TITLE_LENGTH = 255;
	private static final int MAX_DESCRIPTION_LENGTH = 2048;

	@Autowired
	TaskService taskService;

	@GetMapping
	public @ResponseBody List<Task> list(Principal principal) {
		return taskService.listUserTasks(principal.getName());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Task create(@RequestBody Task task, Principal principal) {
		return taskService.createTask(trim(task), principal.getName());
	}

	@PutMapping("/{taskId}")
	public @ResponseBody Task update(@PathVariable Long taskId, @RequestBody Task task, Principal principal) {
		task.setId(taskId);
		return taskService.updateTask(trim(task), principal.getName());
	}

	@PostMapping("/{taskId}/finalized")
	public @ResponseBody Task finalize(@PathVariable Long taskId, Principal principal) {
		return taskService.finalizeTask(taskId, principal.getName());
	}

	@DeleteMapping("/{taskId}")
	public void delete(@PathVariable Long taskId, Principal principal) {
		taskService.deleteTask(taskId, principal.getName());
	}

	private Task trim(Task task) {
		task.setTitle(trim(task.getTitle(), MAX_TITLE_LENGTH));
		task.setDescription(trim(task.getDescription(), MAX_DESCRIPTION_LENGTH));
		return task;
	}

	private String trim(String string, int maxLength) {
		return (string!=null && string.length()>maxLength)? string.substring(0, maxLength): string;
	}
}