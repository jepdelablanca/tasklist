package com.mimacom.tl.mvc;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mimacom.tl.neg.UserService;

@RestController
@Secured("user")
@RequestMapping("/me")
public class MeController {

	@Autowired
	private UserService userService;

	@Value("${tasklist.formLogin}")
	private Boolean formLogin;

	@GetMapping("/name")
	public @ResponseBody String list(Principal principal) {
		return userService.getUserName(principal.getName());
	}

	@GetMapping("/canLogout")
	public @ResponseBody boolean canLogout() {
		return Boolean.TRUE.equals(formLogin);
	}
}