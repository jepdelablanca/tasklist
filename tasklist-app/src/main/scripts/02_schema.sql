
CREATE SCHEMA tasklist AUTHORIZATION tasklist;

CREATE TABLE tasklist.users (
	login VARCHAR(10) NOT NULL,
	password_hash VARCHAR(100) NOT NULL,
	name VARCHAR(128),
	role VARCHAR(10),
	enabled NUMERIC(1, 0),
	
	CONSTRAINT pk_users PRIMARY KEY(login)
);

CREATE TABLE tasklist.tasks (
	id NUMERIC(6, 0) NOT NULL,
	user_id VARCHAR(10) NOT NULL,
	title VARCHAR(255),
	description VARCHAR(2048),
	due_date TIMESTAMP,
	completion_date TIMESTAMP,
	
	CONSTRAINT pk_tasks PRIMARY KEY(id),
	CONSTRAINT fk_tasks_users FOREIGN KEY (user_id) REFERENCES tasklist.users(login)
);
