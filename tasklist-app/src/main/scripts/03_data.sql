
-- Admin user (password: M1m4c0m4dm1n)

INSERT INTO users (login, password_hash, name, role, enabled) 
VALUES ('admin', '$2a$10$.bPpvBon6.XZGh03AhSJKuzMiqm1jBkcwNSBb3U3t.M4BXIxNTjGO', 'Administrator', 'admin', 0);

-- Guest user (password: guest)

INSERT INTO users (login, password_hash, name, role, enabled) 
VALUES ('guest', '$2a$10$kSJguueg9HexMzUb3/5DB.9Fn9ZjFShi21B0upIIPOJsL.yXxwb7K', 'Guest user', 'user', 1);

-- Some tasks

INSERT INTO tasks (id, user_id, title, description, due_date, completion_date) 
VALUES (1, 'guest', 'Tuna pie (recipe)', ''||chr(10)||'- Two cloves of garlic'||chr(10)||'- One onion'||chr(10)||
	'- A red pepper'||chr(10)||'- A green pepper'||chr(10)||'- 100gr of fried tomato'||chr(10)||
	'- Two boiled eggs'||chr(10)||'- A raw egg'||chr(10)||'- 200gr of tuna'||chr(10)||'- Two sheets of puff pastry'||
	chr(10)||chr(10)||'Sauté the minced garlic cloves in oil until golden brown.Add the chopped onion until it is '||
	'poached.Add the chopped peppers. Wait until they are soft.'||chr(10)||chr(10)||'Add the fried tomato, and stir '||
	'frequently until evaporated.'||chr(10)||chr(10)||'Add the chopped boiled egg and the chopped tuna. Stir and '||
	'remove from fire.'||chr(10)||chr(10)||'Spread over a puff pastry sheet evenly. Cover with the other puff pastry '||
	'sheet, and spread the beaten egg.'||chr(10)||chr(10)||'Bake at 180º for 40 minutes.', current_timestamp, current_timestamp);

INSERT INTO tasks (id, user_id,title,description,due_date,completion_date) 
VALUES (2, 'guest', 'Go shopping!', '- 1 garlic'||chr(10)||'- 1 red pepper'||chr(10)||'- 1 green pepper'||chr(10)||
	'- 1 fried tomato brick'||chr(10)||'- eggs'||chr(10)||'- 2 cans of tuna'||chr(10)||'- 2 sheets of puff pastry', current_timestamp + INTERVAL '1 day', null);

INSERT INTO tasks (id, user_id,title,description,due_date,completion_date) 
VALUES (3, 'guest', 'Prepare a tuna pie', 'So delicious!', current_timestamp + INTERVAL '2 days', null);

INSERT INTO tasks (id,user_id,title,description,due_date,completion_date) 
VALUES (4, 'admin', 'Admin something', 'Do some admin stuff', null, null);

INSERT INTO tasks (id,user_id,title,description,due_date,completion_date) 
VALUES (5, 'guest', 'Delete me!', 'Delete this task', current_timestamp, null);

INSERT INTO tasks (id,user_id,title,description,due_date,completion_date) 
VALUES (6, 'guest', 'Be happy', 'All day', null, current_timestamp);

