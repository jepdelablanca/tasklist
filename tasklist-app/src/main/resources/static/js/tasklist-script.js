
function documentReady() {
	$("#newTaskDialog").dialog({
		autoOpen: false,
		width: 315,
		height: 500,
		modal: true,
		resizable: false,
		buttons: [],
		open: function(event) {
			let note = $(event.target);
			let degrees = (1 - Math.random() * 2).toFixed(2);
			note.css({"transform": "rotate("+ degrees +"deg)"});
			let dialog = note.parent();
			dialog.find(".ui-dialog-titlebar-close").hide();
			dialog.find(".ui-dialog-titlebar").hide();
			dialog.css("background-color", "transparent");
			dialog.css("border-color", "transparent");
		},
		close: function() {
			$("#new-task .card-title").val("");
			$("#new-task .card-text").val("");
			$("#new-task .due-date").val("");
		}
	});
	$("#new-task .due-date").datepicker({
		dateFormat: "dd-mm-yy"
	});
	$("#addButton").click(function() {
		$("#newTaskDialog").dialog("open");
	});
	$("#refreshButton").click(requestRefreshTasks);
	$("#new-task .create").click(requestCreateTask);
	$("#new-task .cancel").click(function() {
		$("#newTaskDialog").dialog("close");
	});
	$(body).css("opacity", 1);
	requestMyName();
	requestConfig();
	requestRefreshTasks();
}

function requestMyName() {
	$.get("/me/name", null, function(data) {
		let userSpan = $("#user");
		userSpan.text(data);
		userSpan.attr("title", data);
	}, "text").fail(handleError);
}

function requestConfig() {
	$.get("/me/canLogout", null, function(data) {
		let logoutButton = $("#logoutLink");
		if(data===true) {
			logoutButton.show();
		}
	}).fail(handleError);
}

function requestCreateTask() {
	let title = $("#new-task .card-title").val();
	let description = $("#new-task .card-text").val();
	let dueDate = $("#new-task .due-date").datepicker("getDate");
	$.ajax({
		url: "/tasks",
		type: "POST",
		data: JSON.stringify({
			title: title,
			description: description,
			dueDate: dueDate==null? null: dueDate.getTime()
		}),
		processData: false,
		contentType: "application/json",
		success: function(data) {
			addCard(data);
			$("#newTaskDialog").dialog("close");
		},
		error: handleError
	});
}

function requestUpdateTask(taskId) {
	let card = $("#task-"+ taskId);
	let title = card.find(".card-title").val();
	let description = card.find(".card-text").val();
	let dueDate = card.find("input.due-date").datepicker("getDate");
	$.ajax({
		url: "/tasks/"+taskId,
		type: "PUT",
		data: JSON.stringify({
			id: taskId,
			title: title,
			description: description,
			dueDate: dueDate==null? null: dueDate.getTime()
		}),
		processData: false,
		contentType: "application/json",
		success: function(result) {
			updateCard(result);
		},
		error: handleError
	});
}

function requestFinalizeTask(taskId) {
	$.post("/tasks/"+ taskId +"/finalized",
		null,
		function(result) {
			updateCard(result);
		}, 
		"json").fail(handleError);
}

function requestDeleteTask(taskId) {
	$.ajax({
		url: "/tasks/"+taskId,
		type: "DELETE",
		success: function() {
			$("#task-"+ taskId).remove();
		},
		error: handleError
	});
}

function requestRefreshTasks() {
	$.get("/tasks", null, function(data) {
		if(data && data!=null && Array.isArray(data)) {
			$("#board").empty();
			for(let task of data) {
				addCard(task);
			}
		}
	}, "json").fail(handleError);
}

function addCard(task) {
	let code = "<div class='card m-3' id='task-"+ task.id +"' task-id='"+ task.id +"' style='opacity: 0'>"+
		"<input type='text' class='card-title' maxlength='255' />"+
		"<textarea class='card-text' maxlength='2048'></textarea>"+
		"<ul class='list-group list-group-flush'>"+
			"<li class='list-group-item due-date-row'>"+
				"<span class='noselect date-label'>Due: </span>"+
				"<span class='due-date date-value' />"+
				"<input type='text' class='due-date date-value' value='' />"+
			"</li>"+
			"<li class='list-group-item done-date-row'>"+
				"<span class='noselect date-label'>Done: </span><span class='done-date date-value'></span>"+
			"</li>"+
			"<li class='list-group-item'>"+
				"<button type='button' class='btn btn-primary done'>Mark as done</button>"+
				"<button type='button' class='btn btn-dark delete'>Delete</button>"+
			"</li>"+
		"</ul>"+
	"</div>";
	let card = $(code);
	
	$("#board").append(card);
	let dueDatepicker = card.find("input.due-date");
	dueDatepicker.datepicker({
		dateFormat: "dd-mm-yy",
		onSelect: function() {
			let taskId = $(this).parent().parent().parent().attr("task-id");
			requestUpdateTask(taskId);
		}
	});
	card.find(".card-title").change(function() {
		let taskId = $(this).parent().attr("task-id");
		requestUpdateTask(taskId);
	});
	card.find(".card-text").change(function() {
		let taskId = $(this).parent().attr("task-id");
		requestUpdateTask(taskId);
	});
	card.find("input.due-date").change(function() {
		let newValue = $(this).val();
		$(this).datepicker("setDate", newValue);
		let taskId = $(this).parent().parent().parent().attr("task-id");
		requestUpdateTask(taskId);
	});
	card.find(".btn.done").click(function() {
		let taskId = $(this).parent().parent().parent().attr("task-id");
		requestFinalizeTask(taskId);
	});
	card.find(".btn.delete").click(function() {
		let taskId = $(this).parent().parent().parent().attr("task-id");
		requestDeleteTask(taskId);
	});
	updateCard(task);
}

function updateCard(task) {
	if(!task.title) task.title = "";
	if(!task.description) task.description = "";
	let isDone = task.done || false;
	let card = $("#task-"+ task.id);
	card.attr("done", isDone);
	let dueDate = (task.dueDate && task.dueDate!=null)? task.dueDate: null;
	if(dueDate!=null) {
		try {
			dueDate = new Date(dueDate);
		} catch(e) {}
	}
	let doneDate = (task.completionDate && task.completionDate!=null)? task.completionDate: null;
	if(doneDate!=null) {
		try {
			doneDate = new Date(doneDate);
		} catch(e) {}
	}
	let title = card.find(".card-title");
	title.prop("disabled", isDone);
	card.find(".card-text").prop("disabled", isDone);
	if(isDone) {
		card.find("input.due-date.date-value").hide();
		card.find("span.due-date.date-value").show();
		if(dueDate!=null) {
			card.find("span.due-date.date-value").text(formatDate(dueDate));
		} else {
			card.find("span.due-date.date-value").text("");
		}
		card.find(".done-date-row").show();
		if(doneDate!=null) {
			card.find("span.done-date.date-value").text(formatDate(doneDate));
		} else {
			card.find("span.done-date.date-value").text("");
		}
		card.find(".btn.done").hide();
		card.find(".card-text").addClass("task-done");
	} else {
		card.find(".card-text").removeClass("task-done");
		card.find("span.due-date.date-value").hide();
		card.find("input.due-date.date-value").show();
		card.find(".done-date-row").hide();
		card.find(".btn.done").show();
		if(dueDate!=null) {
			let dueDatepicker = card.find("input.due-date");
			setTimeout(function() {
				dueDatepicker.datepicker("setDate", dueDate);
			}, 0);
		}
	}
	title.val(task.title);
	title.attr("title", task.title);
	card.find(".card-text").val(task.description);
	let degrees = (1 - Math.random() * 2).toFixed(2);
	card.css({
		"transform": "rotate("+ degrees +"deg)",
		 "opacity": 1
	});
}

function formatDate(date) {
	return date.getDate()  + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
}

function handleError(xhr) {
	console.log("Error. Status: "+ xhr.status +", text: "+ xhr.text +", responseText: "+ xhr.responseText);
	if(401==xhr.status) {
		document.location.href="/logout";
	}
}
