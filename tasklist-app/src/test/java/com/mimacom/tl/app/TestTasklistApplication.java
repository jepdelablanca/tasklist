package com.mimacom.tl.app;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class TestTasklistApplication {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testContextLoads() {
		assertTrue(true);
	}

	public void testLoginAuthentication() throws Exception {
		mockMvc.perform(SecurityMockMvcRequestBuilders.formLogin().user("guest").password("guest")).
			andExpect(MockMvcResultMatchers.status().isFound()).
			andExpect(MockMvcResultMatchers.redirectedUrl("/")).
			andExpect(SecurityMockMvcResultMatchers.authenticated().withUsername("guest").withRoles("user"));
		mockMvc
			.perform(SecurityMockMvcRequestBuilders.logout())
			.andExpect(MockMvcResultMatchers.status().isFound())
			.andExpect(MockMvcResultMatchers.redirectedUrl("/"));
	}

	public void testLoginAuthenticationFailsOnWrongPassword() throws Exception {
		mockMvc.perform(SecurityMockMvcRequestBuilders.formLogin().user("guest").password("tseug")).
			andExpect(MockMvcResultMatchers.status().is4xxClientError());
	}
}
