package com.mimacom.tl.neg;

import static org.junit.jupiter.api.Assertions.assertThrows;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mimacom.tl.exception.PermissionDeniedException;
import com.mimacom.tl.model.Task;
import com.mimacom.tl.tools.Mocks;

@SpringBootTest
public class TestTaskService {

	@Autowired
	private TaskService taskService;

	@Test
	public void testWhenUpdatingOthersTaskThrowsException() {
		assertThrows(PermissionDeniedException.class, new Executable() {
			@Override
			public void execute() throws Throwable {
				Task maliciousTask = Mocks.goShopping();
				maliciousTask.setId(3l);
				taskService.updateTask(maliciousTask, "guest");
			}
		});
	}

	@Test
	public void testWhenFinalizingOthersTaskThrowsException() {
		assertThrows(PermissionDeniedException.class, new Executable() {
			@Override
			public void execute() throws Throwable {
				taskService.finalizeTask(3l, "guest");
			}
		});
	}

	@Test
	public void testWhenDeletingOthersTaskThrowsException() {
		assertThrows(PermissionDeniedException.class, new Executable() {
			@Override
			public void execute() throws Throwable {
				taskService.deleteTask(3l, "guest");
			}
		});
	}
}