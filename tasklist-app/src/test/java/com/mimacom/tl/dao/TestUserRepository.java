package com.mimacom.tl.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mimacom.tl.model.User;
import com.mimacom.tl.tools.Mocks;

@DataJpaTest
public class TestUserRepository {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private EntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	@Test
	public void testInjectedComponentsAreNotNull() {
		assertNotNull(dataSource);
		assertNotNull(jdbcTemplate);
		assertNotNull(entityManager);
		assertNotNull(userRepository);
	}

	@Test
	public void testDefaultUsersExist() {
		assertNotNull(userRepository.findAll());
		assertEquals(2, StreamSupport.stream(userRepository.findAll().spliterator(), false).count());
	}

	@Test
	public void testGettingUserById() {
		assertNotNull(userRepository.findById("admin").orElseThrow());
	}

	@Test
	public void testDeleting() {
		userRepository.deleteById("admin");
		assertTrue(userRepository.findById("admin").isEmpty());
	}

	@Test
	public void testInserting() {
		userRepository.save(Mocks.jepdelablanca());
		assertFalse(userRepository.findById("jepdelablanca").isEmpty());
	}

	@Test
	public void testUpdating() {
		User original = Mocks.jepdelablanca();
		assertEquals(1, original.isEnabled());
		userRepository.save(original);
		
		User retrieved = userRepository.findById("jepdelablanca").get();
		retrieved.setEnabled(0);
		userRepository.save(retrieved);
		
		User modified = userRepository.findById("jepdelablanca").get();
		assertEquals(0, modified.isEnabled());
	}
}
