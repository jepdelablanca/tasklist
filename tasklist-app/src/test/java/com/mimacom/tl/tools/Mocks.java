package com.mimacom.tl.tools;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.mimacom.tl.model.Task;
import com.mimacom.tl.model.User;

public class Mocks {

	public static User jepdelablanca() {
		User result = new User();
		result.setLogin("jepdelablanca");
		result.setPasswordHash(BCrypt.hashpw("acnalbaledpej", BCrypt.gensalt()));
		result.setRole("user");
		result.setEnabled(1);
		return result;
	}

	public static Task goShopping() {
		Task result = new Task();
		result.setDescription("Go shopping!");
		result.setDescription("Bring:\n\n"
				+ "- garlic\n"
				+ "- onions\n"
				+ "- red pepper\n"
				+ "- green pepper\n"
				+ "- fried tomato\n"
				+ "- eggs\n"
				+ "- tuna\n"
				+ "- puff pastry");
		return result;
	}

	public static Task celebrate() {
		Task result = new Task();
		result.setId(2l);
		result.setUser("guest");
		result.setTitle("Celebrate");
		result.setDescription("Go have some fun!");
		return result;
	}
}
