package com.mimacom.tl.tools;

import java.util.Calendar;
import java.util.Date;

import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Tools {

	private static ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	public static MockHttpSession getGuestSession(MockMvc mockMvc, boolean formLogin) throws Exception {
		if(formLogin) return formLoginSession(mockMvc);
		return basicAuthSession(mockMvc);
	}

	private static MockHttpSession formLoginSession(MockMvc mockMvc) throws Exception {
		return (MockHttpSession)mockMvc.
				perform(SecurityMockMvcRequestBuilders.formLogin().user("guest").password("guest")).
				andReturn().getRequest().getSession();
	}

	private static MockHttpSession basicAuthSession(MockMvc mockMvc) throws Exception {
		return (MockHttpSession)mockMvc.
				perform(MockMvcRequestBuilders.get("/").with(SecurityMockMvcRequestPostProcessors.httpBasic("guest", "guest"))).
				andReturn().getRequest().getSession();
	}

	public static String toJsonString(Object object) throws JsonProcessingException  {
		return mapper.writeValueAsString(object);
	}

	public static <T> T clone(Object object, Class<T> objectClass) throws JsonProcessingException {
		String parsed = mapper.writeValueAsString(object);
		return (T)mapper.readValue(parsed, objectClass);
	}

	public static Date tomorrow() {
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.add(Calendar.DAY_OF_MONTH, 1);
		return tomorrow.getTime();
	}
}