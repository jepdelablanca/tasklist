package com.mimacom.tl.mvc;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.mimacom.tl.tools.Tools;

@SpringBootTest
@AutoConfigureMockMvc
public class TestMeController {

	@Autowired
	private MockMvc mockMvc;

	@Value("${tasklist.formLogin}")
	private Boolean formLogin;

	@Test
	public void testNameRequestReturnsName() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		mockMvc.perform(MockMvcRequestBuilders.get("/me/name").session(session)).
				andExpect(MockMvcResultMatchers.status().isOk()).
				andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Guest user")));
	}

	@Test
	public void testCanLogoutReturnsFormCofig() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		mockMvc.perform(MockMvcRequestBuilders.get("/me/canLogout").session(session)).
				andExpect(MockMvcResultMatchers.status().isOk()).
				andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(formLogin.toString())));
	}
}