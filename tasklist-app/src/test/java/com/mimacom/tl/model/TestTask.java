package com.mimacom.tl.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestTask {

	@Test
	public void whenNewTask_ItIsNotDone() {
		Task task = new Task();
		assertFalse(task.isDone());
	}

	@Test
	public void whenHasCompletionDate_ItIsDone() {
		Task task = new Task();
		task.setCompletionDate(new Date());
		assertTrue(task.isDone());
	}
}
