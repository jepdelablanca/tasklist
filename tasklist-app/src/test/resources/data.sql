
INSERT INTO users (login, password_hash, name, role, enabled) 
VALUES ('admin', '$2a$10$.bPpvBon6.XZGh03AhSJKuzMiqm1jBkcwNSBb3U3t.M4BXIxNTjGO', 'Administrator', 'admin', 0);

INSERT INTO users (login, password_hash, name, role, enabled) 
VALUES ('guest', '$2a$10$kSJguueg9HexMzUb3/5DB.9Fn9ZjFShi21B0upIIPOJsL.yXxwb7K', 'Guest user', 'user', 1);

INSERT INTO tasks (id, user_id, title, description, due_date, completion_date) 
VALUES (1, 'guest', 'Pass the tests', 'Pass all the tests related with users and tasks.', {ts '2020-10-27 23:59:59.999'}, null);

INSERT INTO tasks (id, user_id, title, description, due_date, completion_date) 
VALUES (2, 'guest', 'Celebrate', 'Go have some fun!', null, null);

INSERT INTO tasks (id, user_id, title, description, due_date, completion_date) 
VALUES (3, 'admin', 'Admin something', 'Perform some admin tasks', null, null);
